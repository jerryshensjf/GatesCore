# 时空之门前端代码生成器

### 动词算子式通用代码生成器阵列全面开源

动词算子式通用代码生成器阵列已全面开源。两个Jar软件依赖如下，皆已全部开源：

曲速引擎前端代码生成器：[https://gitee.com/jerryshensjf/WarpEngine](https://gitee.com/jerryshensjf/WarpEngine)

表反射引擎ReflectTable: [https://gitee.com/jerryshensjf/ReflectTable](https://gitee.com/jerryshensjf/ReflectTable)

### 简介
时空之门前端代码生成器，是第四代动词算子式代码生成器，经过彻底的重构的先进动词算子式代码生成器，也是专用的Vue+ElementUI前端代码生成器，可以和多种后端代码生成器搭配。和平之翼和光对前端代码生成的支持是通过引用不同的时空之门的版本实现的。

本代码生成器的代码生成物基于开源软件Vue-Element-Admin,感谢原作者的贡献。至此，动词算子式代码生成器阵列中所有自制的组件都已经开源，最大限度的利于大家的学习。

这个代码生成器其实是完整代码生成器的前端代码生成组件，使用时是和和平之翼代码生成器或者第三代动词算子式代码生成器：光配合使用的。您可以下这些代码生成器使用，其中的前端项目其实是时空之门代码生成器生成的。这时，时空之门的jar包是放在宿主代码生成器的lib里的。‘

时空之门前端代码生成器支持独立的Swing界面，可以独立运行。

研发进展
时空之门前端代码生成器3.0.0 独立版尝鲜版已可以在本站附件处下载。此版本需要装好Java，双击即可运行，是Swing界面的。

### 项目图片

![Image description](https://images.gitee.com/uploads/images/2021/0510/231325_5874da43_1203742.png "tsgate.png")


### 生成器截图

![Image description](https://images.gitee.com/uploads/images/2021/1122/154332_22e8a6ed_1203742.png "GS_intface.png")

### 交流QQ群
动词算子式式代码生成器群  277689737
