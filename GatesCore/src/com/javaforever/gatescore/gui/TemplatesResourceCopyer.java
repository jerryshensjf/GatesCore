package com.javaforever.gatescore.gui;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

public class TemplatesResourceCopyer {
	protected static Logger logger = Logger.getLogger(TemplatesResourceCopyer.class);
	// 下载并解压拷贝的文件
	public void downloadUnpackTemplates(String projectFolderPath) throws Exception{
		 Resource res = new Resource();
		 String myres = new File(".").getAbsolutePath();
		 myres = myres.substring(0,myres.length()-2)+"/templates/FrontEndTemplates.zip";
		 logger.debug("JerryDebug:myres:"+myres);
		 File myResF = Paths.get(myres).toFile();
		 if (myResF.exists()) {
			 File target = new File(projectFolderPath+"/FrontEndTemplates.zip");
			 Files.copy(myResF.toPath(), target.toPath());
		 }else {		 
			 res.downloadRes("/templates/FrontEndTemplates.zip", projectFolderPath+"/FrontEndTemplates.zip");
		 }
		 ZipCompressor compressor = new ZipCompressor();
		 compressor.unZipFilesAndDeleteOrginal(projectFolderPath+"/FrontEndTemplates.zip", projectFolderPath);
	}
}