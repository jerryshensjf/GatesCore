package org.javaforever.gatescore.core;

import java.io.Serializable;
import java.util.Comparator;

public class FrontFieldSerialComparator implements Comparator<FrontField>,Serializable{
	private static final long serialVersionUID = -3073819745256627215L;

	@Override
	public int compare(FrontField o1, FrontField o2) {
		return ((Long)o1.getSerial()).compareTo(o2.getSerial());
	}

}
