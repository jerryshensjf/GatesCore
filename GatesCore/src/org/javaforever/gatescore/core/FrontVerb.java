package org.javaforever.gatescore.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class FrontVerb implements Comparable<FrontVerb>,Cloneable,Serializable{
	private static final long serialVersionUID = -1506662927938459842L;
	protected String verbName;  
	protected FrontMethod method;
	protected String verbComment;
	protected FrontDomain domain;
	protected List<String> additionalImports = new ArrayList<String>();
	protected String label;	
	protected Boolean denied = false;

	public abstract FrontMethod generateControllerMethod() throws Exception;
	public abstract FrontMethod generateApiMethod() throws Exception;
	public abstract FrontCodeBlock generateRouteBlockBlock() throws Exception;

	public String getVerbName() {
		return verbName;
	}
	public void setVerbName(String verbName) {
		this.verbName = verbName;
	}
	public FrontMethod getMethod() {
		return method;
	}
	public void setMethod(FrontMethod method) {
		this.method = method;
	}
	public String getVerbComment() {
		return verbComment;
	}
	public void setVerbComment(String verbComment) {
		this.verbComment = verbComment;
	}
	public List<String> getAdditionalImports() {
		return additionalImports;
	}
	public void setAdditionalImports(List<String> additionalImports) {
		this.additionalImports = additionalImports;
	}
	
	public FrontVerb(FrontDomain domain){
		super();
		this.domain = domain;
	}
	
	public FrontVerb(){
		super();
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.verbName;
	}
	
	@Override
	public int compareTo(FrontVerb o) {
		return this.getVerbName().compareTo(o.getVerbName());
	}
	public FrontDomain getDomain() {
		return domain;
	}
	public void setDomain(FrontDomain domain) {
		this.domain = domain;
	}
	public Boolean getDenied() {
		return denied;
	}
	public void setDenied(Boolean denied) {
		this.denied = denied;
	}
}
