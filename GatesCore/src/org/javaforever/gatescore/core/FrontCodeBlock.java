package org.javaforever.gatescore.core;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import org.javaforever.gatescore.utils.StringUtil;


public class FrontCodeBlock implements Comparable<FrontCodeBlock>,Cloneable,Serializable{
	private static final long serialVersionUID = -6194100172982248851L;
	protected int serial = 1;
	protected Set<FrontSignature> signatures = new TreeSet<FrontSignature>();
	protected String standardName;
	protected String content = "";
	protected String blockComment;
	protected StatementList blockStatementList = new StatementList();

	public String getBlockComment() {
		return blockComment;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setBlockComment(String blockComment) {
		this.blockComment = blockComment;
	}

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getCapFirstMethodName() {
		return StringUtil.capFirst(standardName);
	}
	
	public String getLowerFirstMethodName() {
		return StringUtil.lowerFirst(standardName);
	}

	public String generateMethodString() {
		return null;
	}
	
	public String generateMethodDefinition(){
		return null;
	}

	public String getThrowException() {
		return "Exception";
	}

	public StatementList getMethodStatementList() {
		return blockStatementList;
	}
	
	public StatementList getMethodStatementList(long serial) {
		blockStatementList.setSerial(serial);
		return blockStatementList;
	}

	public void setMethodStatementList(StatementList blockStatementList) {
		this.blockStatementList = blockStatementList;
	}
	
	public String generateBlockContentStringWithSerial() {
		if (this.blockStatementList != null){
			StringBuilder sb = new StringBuilder();
			Collections.sort(this.blockStatementList);
			for (Statement s : this.blockStatementList){
				sb.append("\t\t"+s.getSerial()+"\t:\t"+s.getContent()+"\n");
			}
			return sb.toString();
		}
		else return this.content;
	}
	
	public String generateBlockContentString() {
		if (this.blockStatementList != null){
			StringBuilder sb = new StringBuilder();
			Collections.sort(this.blockStatementList);
			for (Statement s : this.blockStatementList){
				for (int i=0;i < s.getIndent();i++) sb.append("\t");
				sb.append(s.getContent()).append("\n");
			}
			return sb.toString();
		}
		else return this.content;
	}

	@Override
	public boolean equals(Object o){
		return this.getStandardName().equals(((FrontCodeBlock)o).getStandardName());
	}

	@Override
	public int compareTo(FrontCodeBlock o) {
		return this.standardName.compareTo(o.getStandardName());
	}	
}
