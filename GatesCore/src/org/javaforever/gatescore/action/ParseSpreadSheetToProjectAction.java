package org.javaforever.gatescore.action;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontProject;
import org.javaforever.gatescore.poi.SpreadSheetTranslater;

public class ParseSpreadSheetToProjectAction implements Cloneable,Serializable{
	private static final long serialVersionUID = 6540180022628997630L;

	public FrontProject parseProject(String folderPath,String fileName) throws Exception{
		InputStream is = new FileInputStream(folderPath+fileName);
		POIFSFileSystem fs = new POIFSFileSystem(is);
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		
		SpreadSheetTranslater translater = new SpreadSheetTranslater();
		FrontProject fProject = translater.translate(wb);
		fProject.setExcelTemplateFolder(folderPath);
		fProject.setExcelTemplateName(fileName);
		return fProject;
	}
	
	public static String doParseExcel(String excelPath,String excelFileName, String templatesPath,String savePath,boolean showBackendProject,boolean useControllerPrefix) throws Exception {
		return doParseExcel(excelPath,excelFileName,templatesPath,savePath,showBackendProject,false,useControllerPrefix);
	}
	
	public static String doParseExcel(String excelPath,String excelFileName, String templatesPath,String savePath,boolean showBackendProject,boolean useController,boolean useControllerPrefix) throws Exception {
		ParseSpreadSheetToProjectAction action = new ParseSpreadSheetToProjectAction();
		FrontProject project = action.parseProject(excelPath,excelFileName);
		project.setBackendProjectName(project.getStandardName());
		project.setStandardName(project.getStandardName()+"FrontEnd");		
		project.setShowBackendProject(showBackendProject);
		project.setUseController(useController);
		project.setUseControllerPrefix(useControllerPrefix);
		project.setTemplatesPath(templatesPath);
		project.setFolderPath(savePath);
		project.generateProjectZip();
		return project.getStandardName();
	}
	
	public static void main(String [] args) throws Exception{
//		ParseSpreadSheetToProjectAction action = new ParseSpreadSheetToProjectAction();
//		FrontProject project = action.parseProject("/home/jerry/git/GatesCore/GatesCore/src/org/javaforever/gatescore/action/GenerateSample.xls");
//		System.out.println(project.getStandardName());
//		for (FrontDomain d: project.getDomains()) {
//			System.out.println(d.getStandardName());
//		}
//		project.generateProjectFiles();
		String excelFolderPath = "C:/Users/jerry.shen03/git/GatesCore/GatesCore/src/org/javaforever/gatescore/action/";
		String excelFileName = "CompleteOracleEn.xls";
		String templatesPath = "C:/Users/jerry.shen03/git/GatesCore/GatesCore/templates/";
		String savePath = "D:/JerryWork/GatesCore/TestBed/";
		doParseExcel(excelFolderPath,excelFileName,templatesPath,savePath,true,false);
	}
}
