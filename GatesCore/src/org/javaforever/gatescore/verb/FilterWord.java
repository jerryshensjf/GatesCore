package org.javaforever.gatescore.verb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;
import org.javaforever.gatescore.exception.ValidateException;
import org.javaforever.gatescore.utils.StringUtil;

public class FilterWord extends FrontVerb{
	private static final long serialVersionUID = -8576826956697420520L;

	public FilterWord(FrontDomain d) throws ValidateException{
		super(d);	
		this.denied = domain.isVerbDenied("FilterWord");
		this.setVerbName("Filter"+d.getCapFirstPlural() + "Word");
	}
	
	@Override
	public FrontMethod generateControllerMethod()  throws Exception {
		if (this.denied)
			return null;
		else {
			FrontMethod method = new FrontMethod();
			method.setStandardName("filter"+this.domain.getCapFirstPlural()+ "Word");
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L, 0, "filter"+this.domain.getCapFirstPlural() + "Word(){"));
			sList.add(new Statement(2000L,1,"var params0 = this.buildParams(this.listQuery, this.postForm);"));
			sList.add(new Statement(3000L,1,"var query = this.toQueryString(params0);"));
			sList.add(new Statement(4000L,2,"window.location.href=this.baseApi+\""+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/filter"+this.domain.getCapFirstPlural()+"Word?\"+query;"));
			sList.add(new Statement(5000L, 1, "},"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}

	@Override
	public FrontMethod generateApiMethod()  throws Exception {
		return null;
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		return null;
	}

}
