package org.javaforever.gatescore.verb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;
import org.javaforever.gatescore.exception.ValidateException;
import org.javaforever.gatescore.utils.StringUtil;

public class Export extends FrontVerb{
	private static final long serialVersionUID = 1007425592831193320L;

	public Export(FrontDomain d) throws ValidateException{
		super(d);	
		this.denied = domain.isVerbDenied("Export");
		this.setVerbName("Export"+d.getCapFirstPlural());
	}
	
	@Override
	public FrontMethod generateControllerMethod()  throws Exception {
		if (this.denied)
			return null;
		else {
			FrontMethod method = new FrontMethod();
			method.setStandardName("export"+this.domain.getCapFirstPlural());
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L, 0, "export"+this.domain.getCapFirstPlural() + "(){"));
			sList.add(new Statement(2000L,2,"window.location.href=this.baseApi+\""+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/export"+this.domain.getCapFirstPlural()+"\";"));
			sList.add(new Statement(3000L, 1, "},"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}

	@Override
	public FrontMethod generateApiMethod()  throws Exception {
		return null;
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		return null;
	}

}
