package org.javaforever.gatescore.complexverb;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontField;
import org.javaforever.gatescore.core.FrontMethod;

public abstract class DomainFieldVerb implements Comparable<DomainFieldVerb>{
	protected FrontDomain domain;
	protected FrontField field;
	protected String label;
	protected String verbName;
	protected boolean denied = false;  
	
	public abstract FrontMethod generateControllerMethod() throws Exception;
	public abstract FrontMethod generateApiMethod() throws Exception;
	public abstract FrontCodeBlock generateRouteBlockBlock() throws Exception;

	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.verbName;
	}
	public String getVerbName() {
		return verbName;
	}
	public void setVerbName(String verbName) {
		this.verbName = verbName;
	}
	
	@Override
	public int compareTo(DomainFieldVerb o) {
		return this.getVerbName().compareTo(o.getVerbName());
	}
	
	public boolean isDenied() {
		return denied;
	}
	public void setDenied(boolean denied) {
		this.denied = denied;
	}
}
