package org.javaforever.gatescore.vue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.StatementList;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;
import org.javaforever.gatescore.utils.StringUtil;

public class DevEnvJs extends ElementUIPage implements Serializable{
	private static final long serialVersionUID = -4735014352947450105L;
	protected final String stanadardName = "DevEnv";
	protected final String fileName = "dev.env.js"; 
	protected boolean showBackendProject = false;
	protected String backendProjectName;
	protected boolean useController = false;
	protected boolean useControllerPrefix = false;
	protected String  controllerPackageSuffix = "controller";
	protected String frontBaseApi = "";
	
	public boolean isUseController() {
		return useController;
	}

	public void setUseController(boolean useController) {
		this.useController = useController;
	}

	public String getStanadardName() {
		return stanadardName;
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"module.exports = {"));
		sList.add(new Statement(2000L,0,"NODE_ENV: '\"development\"',"));
		sList.add(new Statement(3000L,0,"ENV_CONFIG: '\"dev\"',"));
		if (!StringUtil.isBlank(this.frontBaseApi)) {
			sList.add(new Statement(4000L,0,"BASE_API: '"+this.frontBaseApi+"'"));
		} else if (!showBackendProject) {
			sList.add(new Statement(4000L,0,"BASE_API: '\"http://localhost:8080/\"'"));
		}else {
			if (this.isUseController()) {
				if (this.isUseControllerPrefix()) {
					sList.add(new Statement(4000L,0,"BASE_API: '\"http://localhost:8080/"+this.backendProjectName+"/"+this.getControllerPackageSuffix()+"/\"'"));
				}else {
					sList.add(new Statement(4000L,0,"BASE_API: '\"http://localhost:8080/"+this.backendProjectName+"/\"'"));
				}
			} else {
				sList.add(new Statement(4000L,0,"BASE_API: '\"http://localhost:8080/"+this.backendProjectName+"/facade/\"'"));
			}
		}
		sList.add(new Statement(5000L,0,"}"));

		return WriteableUtil.merge(sList);
	}

	public boolean isShowBackendProject() {
		return showBackendProject;
	}

	public void setShowBackendProject(boolean showBackendProject) {
		this.showBackendProject = showBackendProject;
	}

	public String getBackendProjectName() {
		return backendProjectName;
	}

	public void setBackendProjectName(String backendProjectName) {
		this.backendProjectName = backendProjectName;
	}

	public String getFileName() {
		return fileName;
	}

	public String getControllerPackageSuffix() {
		return controllerPackageSuffix;
	}

	public void setControllerPackageSuffix(String controllerPackageSuffix) {
		this.controllerPackageSuffix = controllerPackageSuffix;
	}

	public boolean isUseControllerPrefix() {
		return useControllerPrefix;
	}

	public void setUseControllerPrefix(boolean useControllerPrefix) {
		this.useControllerPrefix = useControllerPrefix;
	}

	public String getFrontBaseApi() {
		return frontBaseApi;
	}

	public void setFrontBaseApi(String frontBaseApi) {
		this.frontBaseApi = frontBaseApi;
	}
}
