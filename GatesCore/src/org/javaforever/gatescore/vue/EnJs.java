package org.javaforever.gatescore.vue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.StatementList;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;

public class EnJs extends ElementUIPage implements Comparable<EnJs> ,Cloneable,Serializable{
	private static final long serialVersionUID = -7715930714493390663L;
	protected String stanadardName;

	@Override
	public int compareTo(EnJs o) {
		return this.stanadardName.compareTo(o.getStanadardName());
	}

	public String getStanadardName() {
		return stanadardName;
	}

	public void setStanadardName(String stanadardName) {
		this.stanadardName = stanadardName;
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"export default {"));
		sList.add(new Statement(2000L,0,"route: {"));
		sList.add(new Statement(3000L,1,"dashboard: 'Dashboard',"));
		sList.add(new Statement(4000L,1,"introduction: 'Introduction',"));
		sList.add(new Statement(5000L,1,"documentation: 'Documentation',"));
		sList.add(new Statement(6000L,1,"guide: 'Guide',"));
		sList.add(new Statement(7000L,1,"permission: 'Permission',"));
		sList.add(new Statement(8000L,1,"pagePermission: 'Page Permission',"));
		sList.add(new Statement(9000L,1,"directivePermission: 'Directive Permission',"));
		sList.add(new Statement(10000L,1,"icons: 'Icons',"));
		sList.add(new Statement(11000L,1,"components: 'Components',"));
		sList.add(new Statement(12000L,1,"componentIndex: 'Introduction',"));
		sList.add(new Statement(13000L,1,"tinymce: 'Tinymce',"));
		sList.add(new Statement(14000L,1,"markdown: 'Markdown',"));
		sList.add(new Statement(15000L,1,"jsonEditor: 'JSON Editor',"));
		sList.add(new Statement(16000L,1,"dndList: 'Dnd List',"));
		sList.add(new Statement(17000L,1,"splitPane: 'SplitPane',"));
		sList.add(new Statement(18000L,1,"avatarUpload: 'Avatar Upload',"));
		sList.add(new Statement(19000L,1,"dropzone: 'Dropzone',"));
		sList.add(new Statement(20000L,1,"sticky: 'Sticky',"));
		sList.add(new Statement(21000L,1,"countTo: 'CountTo',"));
		sList.add(new Statement(22000L,1,"componentMixin: 'Mixin',"));
		sList.add(new Statement(23000L,1,"backToTop: 'BackToTop',"));
		sList.add(new Statement(24000L,1,"dragDialog: 'Drag Dialog',"));
		sList.add(new Statement(25000L,1,"dragSelect: 'Drag Select',"));
		sList.add(new Statement(26000L,1,"dragKanban: 'Drag Kanban',"));
		sList.add(new Statement(27000L,1,"charts: 'Charts',"));
		sList.add(new Statement(28000L,1,"keyboardChart: 'Keyboard Chart',"));
		sList.add(new Statement(29000L,1,"lineChart: 'Line Chart',"));
		sList.add(new Statement(30000L,1,"mixChart: 'Mix Chart',"));
		sList.add(new Statement(31000L,1,"example: 'Example',"));
		sList.add(new Statement(32000L,1,"nested: 'Nested Routes',"));
		sList.add(new Statement(33000L,1,"menu1: 'Menu 1',"));
		sList.add(new Statement(34000L,1,"'menu1-1': 'Menu 1-1',"));
		sList.add(new Statement(35000L,1,"'menu1-2': 'Menu 1-2',"));
		sList.add(new Statement(36000L,1,"'menu1-2-1': 'Menu 1-2-1',"));
		sList.add(new Statement(37000L,1,"'menu1-2-2': 'Menu 1-2-2',"));
		sList.add(new Statement(38000L,1,"'menu1-3': 'Menu 1-3',"));
		sList.add(new Statement(39000L,1,"menu2: 'Menu 2',"));
		sList.add(new Statement(40000L,1,"Table: 'Table',"));
		sList.add(new Statement(41000L,1,"dynamicTable: 'Dynamic Table',"));
		sList.add(new Statement(42000L,1,"dragTable: 'Drag Table',"));
		sList.add(new Statement(43000L,1,"inlineEditTable: 'Inline Edit',"));
		sList.add(new Statement(44000L,1,"complexTable: 'Complex Table',"));
		sList.add(new Statement(45000L,1,"treeTable: 'Tree Table',"));
		sList.add(new Statement(46000L,1,"customTreeTable: 'Custom TreeTable',"));
		sList.add(new Statement(47000L,1,"tab: 'Tab',"));
		sList.add(new Statement(48000L,1,"form: 'Form',"));
		sList.add(new Statement(49000L,1,"createArticle: 'Create Article',"));
		sList.add(new Statement(50000L,1,"editArticle: 'Edit Article',"));
		sList.add(new Statement(51000L,1,"articleList: 'Article List',"));
		sList.add(new Statement(52000L,1,"errorPages: 'Error Pages',"));
		sList.add(new Statement(53000L,1,"page401: '401',"));
		sList.add(new Statement(54000L,1,"page404: '404',"));
		sList.add(new Statement(55000L,1,"errorLog: 'Error Log',"));
		sList.add(new Statement(56000L,1,"excel: 'Excel',"));
		sList.add(new Statement(57000L,1,"exportExcel: 'Export Excel',"));
		sList.add(new Statement(58000L,1,"selectExcel: 'Export Selected',"));
		sList.add(new Statement(59000L,1,"uploadExcel: 'Upload Excel',"));
		sList.add(new Statement(60000L,1,"zip: 'Zip',"));
		sList.add(new Statement(61000L,1,"pdf: 'PDF',"));
		sList.add(new Statement(62000L,1,"exportZip: 'Export Zip',"));
		sList.add(new Statement(63000L,1,"theme: 'Theme',"));
		sList.add(new Statement(64000L,1,"clipboardDemo: 'Clipboard',"));
		sList.add(new Statement(65000L,1,"i18n: 'I18n',"));
		sList.add(new Statement(66000L,1,"externalLink: 'External Link'"));
		sList.add(new Statement(67000L,0,"},"));
		sList.add(new Statement(68000L,0,"navbar: {"));
		sList.add(new Statement(69000L,1,"logOut: 'Log Out',"));
		sList.add(new Statement(70000L,1,"dashboard: 'Dashboard',"));
		sList.add(new Statement(71000L,1,"github: 'Github',"));
		sList.add(new Statement(72000L,1,"theme: 'Theme',"));
		sList.add(new Statement(73000L,1,"size: 'Global Size'"));
		sList.add(new Statement(74000L,0,"},"));
		sList.add(new Statement(75000L,0,"login: {"));
		sList.add(new Statement(76000L,1,"title: '"+this.stanadardName+"',"));
		sList.add(new Statement(77000L,1,"logIn: 'Log in',"));
		sList.add(new Statement(78000L,1,"username: 'Username',"));
		sList.add(new Statement(79000L,1,"password: 'Password',"));
		sList.add(new Statement(80000L,1,"any: 'any',"));
		sList.add(new Statement(81000L,1,"thirdparty: 'Or connect with',"));
		sList.add(new Statement(82000L,1,"thirdpartyTips: 'Can not be simulated on local, so please combine you own business simulation! ! !'"));
		sList.add(new Statement(83000L,0,"},"));
		sList.add(new Statement(84000L,0,"documentation: {"));
		sList.add(new Statement(85000L,1,"documentation: 'Documentation',"));
		sList.add(new Statement(86000L,1,"github: 'Github Repository'"));
		sList.add(new Statement(87000L,0,"},"));
		sList.add(new Statement(88000L,0,"permission: {"));
		sList.add(new Statement(89000L,1,"roles: 'Your roles',"));
		sList.add(new Statement(90000L,1,"switchRoles: 'Switch roles',"));
		sList.add(new Statement(91000L,1,"tips: 'In some cases it is not suitable to use v-permission, such as element Tab component or el-table-column and other asynchronous rendering dom cases which can only be achieved by manually setting the v-if.'"));
		sList.add(new Statement(92000L,0,"},"));
		sList.add(new Statement(93000L,0,"guide: {"));
		sList.add(new Statement(94000L,1,"description: 'The guide page is useful for some people who entered the project for the first time. You can briefly introduce the features of the project. Demo is based on ',"));
		sList.add(new Statement(95000L,1,"button: 'Show Guide'"));
		sList.add(new Statement(96000L,0,"},"));
		sList.add(new Statement(97000L,0,"components: {"));
		sList.add(new Statement(98000L,1,"documentation: 'Documentation',"));
		sList.add(new Statement(99000L,1,"tinymceTips: 'Rich text editor is a core part of management system, but at the same time is a place with lots of problems. In the process of selecting rich texts, I also walked a lot of detours. The common rich text editors in the market are basically used, and the finally chose Tinymce. See documentation for more detailed rich text editor comparisons and introductions.',"));
		sList.add(new Statement(100000L,1,"dropzoneTips: 'Because my business has special needs, and has to upload images to qiniu, so instead of a third party, I chose encapsulate it by myself. It is very simple, you can see the detail code in @/components/Dropzone.',"));
		sList.add(new Statement(101000L,1,"stickyTips: 'when the page is scrolled to the preset position will be sticky on the top.',"));
		sList.add(new Statement(102000L,1,"backToTopTips1: 'When the page is scrolled to the specified position, the Back to Top button appears in the lower right corner',"));
		sList.add(new Statement(103000L,1,"backToTopTips2: 'You can customize the style of the button, show / hide, height of appearance, height of the return. If you need a text prompt, you can use element-ui el-tooltip elements externally',"));
		sList.add(new Statement(104000L,1,"imageUploadTips: 'Since I was using only the vue@1 version, and it is not compatible with mockjs at the moment, I modified it myself, and if you are going to use it, it is better to use official version.'"));
		sList.add(new Statement(105000L,0,"},"));
		sList.add(new Statement(106000L,0,"table: {"));
		sList.add(new Statement(107000L,1,"dynamicTips1: 'Fixed header, sorted by header order',"));
		sList.add(new Statement(108000L,1,"dynamicTips2: 'Not fixed header, sorted by click order',"));
		sList.add(new Statement(109000L,1,"dragTips1: 'The default order',"));
		sList.add(new Statement(110000L,1,"dragTips2: 'The after dragging order',"));
		sList.add(new Statement(111000L,1,"title: 'Title',"));
		sList.add(new Statement(112000L,1,"importance: 'Imp',"));
		sList.add(new Statement(113000L,1,"type: 'Type',"));
		sList.add(new Statement(114000L,1,"remark: 'Remark',"));
		sList.add(new Statement(115000L,1,"search: 'Search',"));
		sList.add(new Statement(116000L,1,"add: 'Add',"));
		sList.add(new Statement(117000L,1,"export: 'Export',"));
		sList.add(new Statement(118000L,1,"reviewer: 'reviewer',"));
		sList.add(new Statement(119000L,1,"id: 'ID',"));
		sList.add(new Statement(120000L,1,"date: 'Date',"));
		sList.add(new Statement(121000L,1,"author: 'Author',"));
		sList.add(new Statement(122000L,1,"readings: 'Readings',"));
		sList.add(new Statement(123000L,1,"status: 'Status',"));
		sList.add(new Statement(124000L,1,"actions: 'Actions',"));
		sList.add(new Statement(125000L,1,"edit: 'Edit',"));
		sList.add(new Statement(126000L,1,"publish: 'Publish',"));
		sList.add(new Statement(127000L,1,"draft: 'Draft',"));
		sList.add(new Statement(128000L,1,"delete: 'Delete',"));
		sList.add(new Statement(129000L,1,"cancel: 'Cancel',"));
		sList.add(new Statement(130000L,1,"confirm: 'Confirm'"));
		sList.add(new Statement(131000L,0,"},"));
		sList.add(new Statement(132000L,0,"errorLog: {"));
		sList.add(new Statement(133000L,1,"tips: 'Please click the bug icon in the upper right corner',"));
		sList.add(new Statement(134000L,1,"description: 'Now the management system are basically the form of the spa, it enhances the user experience, but it also increases the possibility of page problems, a small negligence may lead to the entire page deadlock. Fortunately Vue provides a way to catch handling exceptions, where you can handle errors or report exceptions.',"));
		sList.add(new Statement(135000L,1,"documentation: 'Document introduction'"));
		sList.add(new Statement(136000L,0,"},"));
		sList.add(new Statement(137000L,0,"excel: {"));
		sList.add(new Statement(138000L,1,"export: 'Export',"));
		sList.add(new Statement(139000L,1,"selectedExport: 'Export Selected Items',"));
		sList.add(new Statement(140000L,1,"placeholder: 'Please enter the file name(default excel-list)'"));
		sList.add(new Statement(141000L,0,"},"));
		sList.add(new Statement(142000L,0,"zip: {"));
		sList.add(new Statement(143000L,1,"export: 'Export',"));
		sList.add(new Statement(144000L,1,"placeholder: 'Please enter the file name(default file)'"));
		sList.add(new Statement(145000L,0,"},"));
		sList.add(new Statement(146000L,0,"pdf: {"));
		sList.add(new Statement(147000L,1,"tips: 'Here we use window.print() to implement the feature of downloading pdf.'"));
		sList.add(new Statement(148000L,0,"},"));
		sList.add(new Statement(149000L,0,"theme: {"));
		sList.add(new Statement(150000L,1,"change: 'Change Theme',"));
		sList.add(new Statement(151000L,1,"documentation: 'Theme documentation',"));
		sList.add(new Statement(152000L,1,"tips: 'Tips: It is different from the theme-pick on the navbar is two different skinning methods, each with different application scenarios. Refer to the documentation for details.'"));
		sList.add(new Statement(153000L,0,"},"));
		sList.add(new Statement(154000L,0,"tagsView: {"));
		sList.add(new Statement(155000L,1,"refresh: 'Refresh',"));
		sList.add(new Statement(156000L,1,"close: 'Close',"));
		sList.add(new Statement(157000L,1,"closeOthers: 'Close Others',"));
		sList.add(new Statement(158000L,1,"closeAll: 'Close All'"));
		sList.add(new Statement(159000L,0,"}"));
		sList.add(new Statement(160000L,0,"}"));
		return WriteableUtil.merge(sList);
	}
}
