 	package org.javaforever.gatescore.vue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.javaforever.gatescore.complexverb.FrontTwinsVerb;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontDropdown;
import org.javaforever.gatescore.core.FrontField;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVar;
import org.javaforever.gatescore.core.FrontVerb;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.StatementList;
import org.javaforever.gatescore.core.UtilsMethodGenerator;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;
import org.javaforever.gatescore.verb.ListActive;

public class ElementUIGridPage extends ElementUIPage implements Comparable<ElementUIGridPage>,Cloneable,Serializable{
	private static final long serialVersionUID = -3087125674099161920L;
	protected FrontDomain domain;
	protected String stanadardName;
	protected Long serial = 0L;
	protected Set<FrontVerb> verbs = new TreeSet<FrontVerb>();
	protected Set<FrontTwinsVerb> twinsverbs = new TreeSet<FrontTwinsVerb>();

	@Override
	public int compareTo(ElementUIGridPage o) {
		return this.stanadardName.compareTo(o.getStanadardName());
	}
	
	public ElementUIGridPage(FrontDomain domain) {
		super();
		this.domain = domain;
	}

	public FrontDomain getDomain() {
		return domain;
	}

	public void setDomain(FrontDomain domain) {
		this.domain = domain;
	}

	public String getStanadardName() {
		return stanadardName;
	}

	public void setStanadardName(String stanadardName) {
		this.stanadardName = stanadardName;
	}

	public Long getSerial() {
		return serial;
	}

	public void setSerial(Long serial) {
		this.serial = serial;
	}

	public Set<FrontVerb> getVerbs() {
		return verbs;
	}

	public void setVerbs(Set<FrontVerb> verbs) {
		this.verbs = verbs;
	}

	@Override
	public StatementList generateStatementList() throws Exception{
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<template>"));
		sList.add(new Statement(2000L,0,"<div class=\"app-container\">"));
		sList.add(new Statement(3000L,0,"<el-form ref=\"postForm\" :model=\"postForm\" class=\"form-container\">"));
		sList.add(new Statement(4000L,1,"<div class=\"createPost-main-container\">"));
		
		serial = 5000L;
		Set<FrontField> set = this.domain.getSearchFields();
		List<FrontField> list = new ArrayList<FrontField>();
		list.addAll(set);
		for (int i=0;i<set.size();i++) {
			if (i%4==0) {
				sList.add(new Statement(serial,1,"<el-row>"));
				serial += 1000L;
			}
			FrontField f = list.get(i);
			if (!(f instanceof FrontDropdown)) {				
				sList.add(new Statement(serial,2,"<el-col :span=\"4\">"));
				sList.add(new Statement(serial+1000L,2,"<el-form-item label-width=\"120px\" label=\""+f.getText()+"\" class=\"postInfo-container-item\">"));
				sList.add(new Statement(serial+2000L,3,"<el-input v-model=\"postForm."+f.getFieldName()+"\" label=\""+f.getText()+"\" style=\"width: 160px;\">"));
				sList.add(new Statement(serial+3000L,3,"</el-input>"));
				sList.add(new Statement(serial+4000L,2,"</el-form-item>"));
				sList.add(new Statement(serial+5000L,2,"</el-col>"));
				serial = serial+12000L;
			} else {
				FrontDropdown dp = (FrontDropdown)f;
				sList.add(new Statement(serial,2,"<el-col :span=\"4\">"));
				sList.add(new Statement(serial+1000L,2,"<el-form-item label-width=\"120px\" label=\""+dp.getText()+"\" class=\"postInfo-container-item\" prop=\""+dp.getAliasOrName()+"\">"));
				if (this.domain.getLanguage().equalsIgnoreCase("english")) {
					sList.add(new Statement(serial+2000L,3,"<el-select v-model=\"postForm."+dp.getLowerFirstFieldName()+"\" value-key=\""+dp.getTarget().getDomainId().getFieldName()+"\" placeholder=\"Please choose\" style=\"width: 160px;\"  @change=\"translate"+dp.getTarget().getCapFirstDomainName()+"\">"));
				}else {
					sList.add(new Statement(serial+2000L,3,"<el-select v-model=\"postForm."+dp.getLowerFirstFieldName()+"\" value-key=\""+dp.getTarget().getDomainId().getFieldName()+"\" placeholder=\"请选择\" style=\"width: 160px;\"  @change=\"translate"+dp.getTarget().getCapFirstDomainName()+"\">"));
				}
				sList.add(new Statement(serial+3000L,3,"<el-option"));
				sList.add(new Statement(serial+4000L,4,"v-for=\"item in active"+dp.getTarget().getCapFirstPlural()+"\""));
				sList.add(new Statement(serial+5000L,4,":key=\"item."+dp.getTarget().getDomainId().getFieldName()+"\""));
				sList.add(new Statement(serial+6000L,4,":label=\"item."+dp.getTarget().getDomainName().getFieldName()+"\""));
				sList.add(new Statement(serial+7000L,4,":value=\"item."+dp.getTarget().getDomainId().getFieldName()+"\">"));
				sList.add(new Statement(serial+8000L,3,"</el-option>"));
				sList.add(new Statement(serial+9000L,3,"</el-select>"));
				sList.add(new Statement(serial+10000L,2,"</el-form-item>"));
				sList.add(new Statement(serial+11000L,2,"</el-col>"));
				serial = serial+12000L;
			}	
			if (i==3) {
				sList.add(new Statement(serial,2,"<el-col :span=\"4\">"));
				sList.add(new Statement(serial+500L,2,"<el-form-item label-width=\"10px\" class=\"postInfo-container-item\">"));
				if (this.domain.getLanguage().equalsIgnoreCase("english")) {
					sList.add(new Statement(serial+1000L,2,"<el-dropdown @command=\"searchFilterFun\">"));
					sList.add(new Statement(serial+2000L,3,"<el-button type=\"primary\" @click=\"this.search"+this.domain.getCapFirstPlural()+"ByFieldsByPage\" >"));
					sList.add(new Statement(serial+3000L,4,"<span >Search</span><i class=\"el-icon-arrow-down el-icon--right\"></i>"));
					sList.add(new Statement(serial+4000L,3,"</el-button>"));
					sList.add(new Statement(serial+5000L,3,"<el-dropdown-menu slot=\"dropdown\">"));
					if (!this.domain.isVerbDenied("FilterExcel")) {
						sList.add(new Statement(serial+6000L,4,"<el-dropdown-item command="));
						sList.add(new Statement(serial+7000L,4,"\"Excel\">FilterExcel</el-dropdown-item>"));
					}
					if (!this.domain.isVerbDenied("FilterPDF")) {
						sList.add(new Statement(serial+8000L,4,"<el-dropdown-item command="));
						sList.add(new Statement(serial+9000L,4,"\"PDF\">FilterPDF</el-dropdown-item>"));
					}
					if (!this.domain.isVerbDenied("FilterWord")) {
						sList.add(new Statement(serial+10000L,4,"<el-dropdown-item command="));
						sList.add(new Statement(serial+11000L,4,"\"Word\">FilterWord</el-dropdown-item>"));
					}
					sList.add(new Statement(serial+12000L,3,"</el-dropdown-menu>"));
					sList.add(new Statement(serial+13000L,2,"</el-dropdown>"));
				}else {
					sList.add(new Statement(serial+1000L,2,"<el-dropdown @command=\"searchFilterFun\">"));
					sList.add(new Statement(serial+2000L,3,"<el-button type=\"primary\" @click=\"this.search"+this.domain.getCapFirstPlural()+"ByFieldsByPage\" >"));
					sList.add(new Statement(serial+3000L,4,"<span >搜索</span><i class=\"el-icon-arrow-down el-icon--right\"></i>"));
					sList.add(new Statement(serial+4000L,3,"</el-button>"));
					sList.add(new Statement(serial+5000L,3,"<el-dropdown-menu slot=\"dropdown\">"));
					if (!this.domain.isVerbDenied("FilterExcel")) {
						sList.add(new Statement(serial+6000L,4,"<el-dropdown-item command="));
						sList.add(new Statement(serial+7000L,4,"\"Excel\">Excel过滤</el-dropdown-item>"));
					}
					if (!this.domain.isVerbDenied("FilterPDF")) {
						sList.add(new Statement(serial+8000L,4,"<el-dropdown-item command="));
						sList.add(new Statement(serial+9000L,4,"\"PDF\">PDF过滤</el-dropdown-item>"));
					}
					if (!this.domain.isVerbDenied("FilterWord")) {
						sList.add(new Statement(serial+10000L,4,"<el-dropdown-item command="));
						sList.add(new Statement(serial+11000L,4,"\"Word\">Word过滤</el-dropdown-item>"));
					}
					sList.add(new Statement(serial+12000L,3,"</el-dropdown-menu>"));
					sList.add(new Statement(serial+13000L,2,"</el-dropdown>"));
				}
				sList.add(new Statement(serial+14000L,2,"</el-form-item>"));
				sList.add(new Statement(serial+15000L,2,"</el-col>"));
				serial += 16000L;
			}
			if (i%4==3) {
				sList.add(new Statement(serial,1,"</el-row>"));
				serial += 1000L;
			}
		}
		for (int i=set.size()%4;i<4;i++) {
			sList.add(new Statement(serial,2,"<el-col :span=\"4\">"));
			sList.add(new Statement(serial+1000L,2,"</el-col>"));
			serial += 2000L;
			if (i==3&&set.size()<4) {
				sList.add(new Statement(serial,2,"<el-col :span=\"4\">"));
				sList.add(new Statement(serial+1000L,2,"<el-form-item label-width=\"10px\" class=\"postInfo-container-item\">"));
				if (this.domain.getLanguage().equalsIgnoreCase("english")) {
					sList.add(new Statement(serial+2000L,3,"<el-button type=\"primary\" icon=\"el-icon-search\" style=\"width: 120px;\" 　@click=\"this.search"+this.domain.getCapFirstPlural()+"ByFieldsByPage\">Search</el-button>"));
				}else {
					sList.add(new Statement(serial+2000L,3,"<el-button type=\"primary\" icon=\"el-icon-search\" style=\"width: 120px;\" 　@click=\"this.search"+this.domain.getCapFirstPlural()+"ByFieldsByPage\">搜索</el-button>"));
				}
				sList.add(new Statement(serial+3000L,2,"</el-form-item>"));
				sList.add(new Statement(serial+4000L,2,"</el-col>"));
				serial += 5000L;
			}
			if (i%4==3) {
				sList.add(new Statement(serial,"</el-row>"));
				serial += 1000L;
			}
		}
		sList.add(new Statement(serial+1000L,1,"</div>"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+2500L,0,"<el-button-group>"));
		if (this.domain.getLanguage().equalsIgnoreCase("english")) {
			if (!this.domain.isVerbDenied("Add")) sList.add(new Statement(serial+3000L,1,"<el-button size=\"mini\" type=\"success\" icon=\"el-icon-plus\" @click=\"showAddDialog\">Add</el-button>"));
			if (!this.domain.isVerbDenied("View")) sList.add(new Statement(serial+3500L,1,"<el-button size=\"mini\" type=\"success\" icon=\"el-icon-search\" @click=\"showViewDialog\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\">View</el-button>"));
			if (!this.domain.isVerbDenied("Update")) sList.add(new Statement(serial+4000L,1,"<el-button size=\"mini\" type=\"success\" icon=\"el-icon-edit\" @click=\"showEditDialog\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\">Update</el-button>"));
			if (!this.domain.isVerbDenied("Activate")) sList.add(new Statement(serial+5000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-star-off\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\" 　@click=\"activate"+this.domain.getCapFirstDomainName()+"\">Activate</el-button>"));
			if (!this.domain.isVerbDenied("Toggle")) sList.add(new Statement(serial+6000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-sort\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\" 　@click=\"toggle"+this.domain.getCapFirstDomainName()+"\">Toggle</el-button>"));
			if (!this.domain.isVerbDenied("ToggleOne")) sList.add(new Statement(serial+7000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-sort\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\" 　@click=\"toggleOne"+this.domain.getCapFirstDomainName()+"\">Toggle One</el-button>"));
			if (!this.domain.isVerbDenied("SoftDelete")) sList.add(new Statement(serial+8000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-delete\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\" 　@click=\"softDelete"+this.domain.getCapFirstDomainName()+"\">Soft Delete</el-button>"));
			if (!this.domain.isVerbDenied("Clone")) sList.add(new Statement(serial+8500L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-plus\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\" 　@click=\"clone"+this.domain.getCapFirstDomainName()+"\">Clone</el-button>"));
			if (!this.domain.isVerbDenied("Delete")) sList.add(new Statement(serial+9000L,1,"<el-button size=\"mini\" type=\"danger\" icon=\"el-icon-delete\" :disabled=\"this.selectList.length==0\" @click=\"delete"+this.domain.getCapFirstDomainName()+"\">Delete</el-button>"));
			if (!this.domain.isVerbDenied("ActivateAll")) sList.add(new Statement(serial+10000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-star-off\" :disabled=\"this.selectList.length==0\" @click=\"activateAll"+this.domain.getCapFirstPlural()+"\">Batch Activate</el-button>"));
			if (!this.domain.isVerbDenied("SoftDeleteAll")) sList.add(new Statement(serial+11000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-delete\" :disabled=\"this.selectList.length==0\" @click=\"softDeleteAll"+this.domain.getCapFirstPlural()+"\">Batch Soft Delete</el-button>"));
			if (!this.domain.isVerbDenied("CloneAll")) sList.add(new Statement(serial+11500L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-plus\" :disabled=\"this.selectList.length==0\" @click=\"cloneAll"+this.domain.getCapFirstPlural()+"\">Batch Clone</el-button>"));
			if (!this.domain.isVerbDenied("DeleteAll")) sList.add(new Statement(serial+12000L,1,"<el-button size=\"mini\" type=\"danger\" icon=\"el-icon-delete\" :disabled=\"this.selectList.length==0\" @click=\"deleteAll"+this.domain.getCapFirstPlural()+"\">Batch Delete</el-button>"));
			if (!this.domain.isVerbDenied("Export")) sList.add(new Statement(serial+13000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-date\" @click=\"export"+this.domain.getCapFirstPlural()+"\">Excel</el-button>"));
			if (!this.domain.isVerbDenied("ExportPDF")) sList.add(new Statement(serial+14000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-edit-outline\" @click=\"export"+this.domain.getCapFirstPlural()+"PDF\">PDF</el-button>"));
			if (!this.domain.isVerbDenied("ExportWord")) sList.add(new Statement(serial+15000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-upload\" @click=\"export"+this.domain.getCapFirstPlural()+"Word\">Word</el-button>"));
		}else {
			if (!this.domain.isVerbDenied("Add")) sList.add(new Statement(serial+3000L,1,"<el-button size=\"mini\" type=\"success\" icon=\"el-icon-plus\" @click=\"showAddDialog\">添加</el-button>"));
			if (!this.domain.isVerbDenied("View")) sList.add(new Statement(serial+3500L,1,"<el-button size=\"mini\" type=\"success\" icon=\"el-icon-search\" @click=\"showViewDialog\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\">查看</el-button>"));
			if (!this.domain.isVerbDenied("Update")) sList.add(new Statement(serial+4000L,1,"<el-button size=\"mini\" type=\"success\" icon=\"el-icon-edit\" @click=\"showEditDialog\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\">编辑</el-button>"));
			if (!this.domain.isVerbDenied("Activate")) sList.add(new Statement(serial+5000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-star-off\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\" 　@click=\"activate"+this.domain.getCapFirstDomainName()+"\">激活</el-button>"));
			if (!this.domain.isVerbDenied("Toggle")) sList.add(new Statement(serial+6000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-sort\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\" 　@click=\"toggle"+this.domain.getCapFirstDomainName()+"\">切换</el-button>"));
			if (!this.domain.isVerbDenied("ToggleOne")) sList.add(new Statement(serial+7000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-sort\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\" 　@click=\"toggleOne"+this.domain.getCapFirstDomainName()+"\">留一切换</el-button>"));
			if (!this.domain.isVerbDenied("SoftDelete")) sList.add(new Statement(serial+8000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-delete\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\" 　@click=\"softDelete"+this.domain.getCapFirstDomainName()+"\">软删除</el-button>"));
			if (!this.domain.isVerbDenied("Clone")) sList.add(new Statement(serial+8500L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-plus\" :disabled=\"this.selectList.length==0 || this.selectList.length > 1\" 　@click=\"clone"+this.domain.getCapFirstDomainName()+"\">克隆</el-button>"));
			if (!this.domain.isVerbDenied("Delete")) sList.add(new Statement(serial+9000L,1,"<el-button size=\"mini\" type=\"danger\" icon=\"el-icon-delete\" :disabled=\"this.selectList.length==0\" @click=\"delete"+this.domain.getCapFirstDomainName()+"\">删除</el-button>"));
			if (!this.domain.isVerbDenied("ActivateAll")) sList.add(new Statement(serial+10000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-star-off\" :disabled=\"this.selectList.length==0\" @click=\"activateAll"+this.domain.getCapFirstPlural()+"\">批激活</el-button>"));
			if (!this.domain.isVerbDenied("SoftDeleteAll")) sList.add(new Statement(serial+11000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-delete\" :disabled=\"this.selectList.length==0\" @click=\"softDeleteAll"+this.domain.getCapFirstPlural()+"\">批软删除</el-button>"));
			if (!this.domain.isVerbDenied("CloneAll")) sList.add(new Statement(serial+11500L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-plus\" :disabled=\"this.selectList.length==0\" @click=\"cloneAll"+this.domain.getCapFirstPlural()+"\">批克隆</el-button>"));
			if (!this.domain.isVerbDenied("DeleteAll")) sList.add(new Statement(serial+12000L,1,"<el-button size=\"mini\" type=\"danger\" icon=\"el-icon-delete\" :disabled=\"this.selectList.length==0\" @click=\"deleteAll"+this.domain.getCapFirstPlural()+"\">批删除</el-button>"));
			if (!this.domain.isVerbDenied("Export")) sList.add(new Statement(serial+13000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-date\" @click=\"export"+this.domain.getCapFirstPlural()+"\">Excel</el-button>"));
			if (!this.domain.isVerbDenied("ExportPDF")) sList.add(new Statement(serial+14000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-edit-outline\" @click=\"export"+this.domain.getCapFirstPlural()+"PDF\">PDF</el-button>"));
			if (!this.domain.isVerbDenied("ExportWord")) sList.add(new Statement(serial+15000L,1,"<el-button size=\"mini\" type=\"primary\" icon=\"el-icon-upload\" @click=\"export"+this.domain.getCapFirstPlural()+"Word\">Word</el-button>"));
		}
		sList.add(new Statement(serial+15500L,0,"</el-button-group>"));
		sList.add(new Statement(serial+16000L,0,"</el-form>"));
		sList.add(new Statement(serial+17000L,0,"<br />"));
		sList.add(new Statement(serial+18000L,0,""));
		sList.add(new Statement(serial+19000L,0,"<el-table ref=\"table\" v-loading=\"listLoading\" :data=\"list\" border fit highlight-current-row style=\"width: 100%\" :row-style=\"selectedHighlight\" 　@select-all=\"handleCheckedAll\" 　@selection-change=\"handleChecked\""));
		sList.add(new Statement(serial+20000L,1,":header-cell-style=\"{'background-color': '#3b8fce','color':'white'}\">"));
		sList.add(new Statement(serial+21000L,0,""));
		sList.add(new Statement(serial+22000L,1,"<el-table-column type=\"selection\" align=\"center\" width=\"40px\">"));
		sList.add(new Statement(serial+23000L,1,"</el-table-column>"));
		sList.add(new Statement(serial+24000L,0,""));
		FrontField fid = this.domain.getDomainId()==null?null: this.domain.getDomainId();
		if (fid != null ) {
			sList.add(new Statement(serial+25000L,1,"<el-table-column width=\"80px\" align=\"center\" label=\""+fid.getText()+"\">"));
			sList.add(new Statement(serial+26000L,1,"<template slot-scope=\"scope\">"));
			sList.add(new Statement(serial+27000L,2,"<span>{{ scope.row."+fid.getFieldName()+" }}</span>"));
			sList.add(new Statement(serial+28000L,1,"</template>"));
			sList.add(new Statement(serial+29000L,1,"</el-table-column>"));
			sList.add(new Statement(serial+30000L,0,""));
		}
		serial = serial+31000L;
		for (FrontField f:this.domain.getAllFieldsWithoutId()) {
			if (!(f instanceof FrontDropdown)) {
			if (f.getFieldType()!=null&&f.getFieldType().getTypeName()!=null&&f.getFieldType().getTypeName().equalsIgnoreCase("image")) {
					sList.add(new Statement(serial,1,"<el-table-column width=\"160px\" align=\"center\" label=\""+f.getText()+"\" :show-overflow-tooltip=\"true\">"));
					sList.add(new Statement(serial+1000L,2,"<template slot-scope=\"scope\">"));
					sList.add(new Statement(serial+2000L,2,"<img v-if=\"!scope.row."+f.getLowerFirstFieldName()+"\"　:src=\"'../src/views/images/blank.jpg'\" height=\"50\"/>"));
					sList.add(new Statement(serial+3000L,2,"<img v-else :src=\"'data:image/png;base64,'+scope.row."+f.getLowerFirstFieldName()+"\" height=\"50\"/>"));
					sList.add(new Statement(serial+4000L,1,"</template>"));
					sList.add(new Statement(serial+5000L,1,"</el-table-column>"));
				}else {
					sList.add(new Statement(serial,1,"<el-table-column width=\"160px\" align=\"center\" label=\""+f.getText()+"\">"));
					sList.add(new Statement(serial+1000L,1,"<template slot-scope=\"scope\">"));
					sList.add(new Statement(serial+2000L,2,"<span>{{ scope.row."+f.getFieldName()+" }}</span>"));
					sList.add(new Statement(serial+3000L,1,"</template>"));
					sList.add(new Statement(serial+4000L,1,"</el-table-column>"));
					sList.add(new Statement(serial+5000L,0,""));
				}
				serial = serial + 6000L;
			}else {
				FrontDropdown dp = (FrontDropdown)f;
				sList.add(new Statement(serial,1,"<el-table-column width=\"160px\" align=\"center\" label=\""+dp.getText()+"\">"));
				sList.add(new Statement(serial+1000L,1,"<template slot-scope=\"scope\">"));
				sList.add(new Statement(serial+2000L,2,"{{ scope.row."+dp.getAliasName()+"  | "+dp.getTarget().getLowerFirstDomainName()+"Filter}}"));
				sList.add(new Statement(serial+3000L,1,"</template>"));
				sList.add(new Statement(serial+4000L,1,"</el-table-column>"));
				sList.add(new Statement(serial+5000L,0,""));
				serial = serial + 6000L;
			}			
		}
		sList.add(new Statement(serial,0,"</el-table>"));
		sList.add(new Statement(serial+1000L,0,""));
		sList.add(new Statement(serial+2000L,0,"<pagination v-show=\"total>0\" :total=\"total\" :page.sync=\"listQuery.page\" :limit.sync=\"listQuery.rows\" @pagination=\"search"+this.domain.getCapFirstPlural()+"ByFieldsByPage\" />"));
		sList.add(new Statement(serial+3000L,0,""));
		
		if (!this.domain.isVerbDenied("Add")) {
			sList.add(new Statement(serial+4000L,0,"<el-dialog"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sList.add(new Statement(serial+5000L,1,"title=\"Add\""));
			}else {
				sList.add(new Statement(serial+5000L,1,"title=\"新增\""));
			}
			sList.add(new Statement(serial+6000L,1,":visible.sync=\"addDialogVisible\""));
			sList.add(new Statement(serial+7000L,1,"width=\"850px\""));
			sList.add(new Statement(serial+8000L,1,":before-close=\"handleClose\">"));
			sList.add(new Statement(serial+9000L,1,"<el-form  :model=\"form\" label-position=\"right\" label-width=\"120px\">"));
			sList.add(new Statement(serial+10000L,1,"<el-row>"));
			serial = serial + 11000L;
			for (FrontField f:this.domain.getAllFieldsWithoutId()) {
				if (!(f instanceof FrontDropdown)) {
					if (f.getFieldType().getTypeName().equalsIgnoreCase("image")) {
						sList.add(new Statement(serial+1000L,1,"<el-col :span=\"12\">"));
						sList.add(new Statement(serial+2000L,2,"<el-form-item label=\""+f.getText()+"\">"));
						sList.add(new Statement(serial+3000L,1,"<el-upload"));
						sList.add(new Statement(serial+4000L,2,"action=\"this.baseApi+'"+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/addUpload"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"'\""));
						sList.add(new Statement(serial+5000L,2,":show-file-list=\"false\""));
						sList.add(new Statement(serial+6000L,2,":on-success=\"handle"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"ImageSuccess\""));
						sList.add(new Statement(serial+7000L,2,":before-upload=\"before"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"ImageUpload\">"));
						sList.add(new Statement(serial+8000L,4,"<img v-if=\""+f.getLowerFirstFieldName()+"ImageUrl\" :src=\""+f.getLowerFirstFieldName()+"ImageUrl\" width=\"300\"/>"));
						sList.add(new Statement(serial+9000L,4,"<img v-else :src=\"'../src/views/images/blank.jpg'\" width=\"300\"/>"));
						sList.add(new Statement(serial+10000L,1,"<br>"));
						if (this.domain.getLanguage().equalsIgnoreCase("english")) {
							sList.add(new Statement(serial+11000L,4,"<el-button type=\"primary\" plain v-on:click=\"up"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"ImageData\">Upload</el-button>"));
						}else {
							sList.add(new Statement(serial+11000L,4,"<el-button type=\"primary\" plain v-on:click=\"up"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"ImageData\">上传</el-button>"));
						}
						sList.add(new Statement(serial+12000L,1,"</el-upload>"));
						sList.add(new Statement(serial+13000L,2,"</el-form-item>"));
						sList.add(new Statement(serial+14000L,2,"</el-col>"));
					}else {
						sList.add(new Statement(serial,2,"<el-col :span=\"12\">"));
						sList.add(new Statement(serial+1000L,2,"<el-form-item label=\""+f.getText()+"\">"));
						sList.add(new Statement(serial+2000L,3,"<el-input v-model=\"form."+f.getFieldName()+"\" style=\"width: 240px;\"></el-input>"));
						sList.add(new Statement(serial+3000L,2,"</el-form-item>"));
						sList.add(new Statement(serial+4000L,2,"</el-col>"));
					}
					serial = serial + 15000L;
				}else {
					FrontDropdown dp = (FrontDropdown)f;
					sList.add(new Statement(serial,2,"<el-col :span=\"12\">"));
					sList.add(new Statement(serial+1000L,2,"<el-form-item label=\""+dp.getText()+"\" prop=\""+dp.getAliasOrName()+"\">"));
					if (this.domain.getLanguage().equalsIgnoreCase("english")) {
						sList.add(new Statement(serial+2000L,3,"<el-select v-model=\"form."+dp.getAliasName()+"\" value-key=\""+dp.getTarget().getDomainId().getFieldName()+"\" placeholder=\"Please choose \" style=\"width: 240px;\" @change=\"translate"+dp.getTarget().getCapFirstDomainName()+"\">"));
					}else{
						sList.add(new Statement(serial+2000L,3,"<el-select v-model=\"form."+dp.getAliasName()+"\" value-key=\""+dp.getTarget().getDomainId().getFieldName()+"\" placeholder=\"请选择\" style=\"width: 240px;\" @change=\"translate"+dp.getTarget().getCapFirstDomainName()+"\">"));
					}
					sList.add(new Statement(serial+3000L,3,"<el-option"));
					sList.add(new Statement(serial+4000L,4,"v-for=\"item in active"+dp.getTarget().getCapFirstPlural()+"\""));
					sList.add(new Statement(serial+5000L,4,":key=\"item."+dp.getAliasName()+"\""));
					sList.add(new Statement(serial+6000L,4,":label=\"item."+dp.getTarget().getDomainName().getFieldName()+"\""));
					sList.add(new Statement(serial+7000L,4,":value=\"item."+dp.getTarget().getDomainId().getFieldName()+"\">"));
					sList.add(new Statement(serial+8000L,3,"</el-option>"));
					sList.add(new Statement(serial+9000L,3,"</el-select>"));
					sList.add(new Statement(serial+10000L,2,"</el-form-item>"));
					sList.add(new Statement(serial+11000L,2,"</el-col>"));
					serial = serial + 15000L;
				}			
			}
			sList.add(new Statement(serial,1,"</el-row>"));
			sList.add(new Statement(serial+1000L,1,"</el-form>"));
			sList.add(new Statement(serial+2000L,1,"<span slot=\"footer\" class=\"dialog-footer\">"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sList.add(new Statement(serial+3000L,1,"<el-button @click=\"addDialogVisible = false\">Cancel</el-button>"));
				sList.add(new Statement(serial+4000L,1,"<el-button type=\"primary\" @click=\"add"+this.domain.getCapFirstDomainName()+"\">Add</el-button>"));
			}else {
				sList.add(new Statement(serial+3000L,1,"<el-button @click=\"addDialogVisible = false\">取 消</el-button>"));
				sList.add(new Statement(serial+4000L,1,"<el-button type=\"primary\" @click=\"add"+this.domain.getCapFirstDomainName()+"\">新增</el-button>"));
			}
			sList.add(new Statement(serial+5000L,1,"</span>"));
			sList.add(new Statement(serial+6000L,0,"</el-dialog>"));
			sList.add(new Statement(serial+7000L,0,""));
		}
		
		if (!this.domain.isVerbDenied("Update")) {
			sList.add(new Statement(serial+8000L,0,"<el-dialog"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sList.add(new Statement(serial+9000L,1,"title=\"Update\""));
			}else {
				sList.add(new Statement(serial+9000L,1,"title=\"编辑\""));
			}
			sList.add(new Statement(serial+10000L,1,":visible.sync=\"editDialogVisible\""));
			sList.add(new Statement(serial+11000L,1,"width=\"850px\""));
			sList.add(new Statement(serial+12000L,1,":before-close=\"handleClose\">"));
			sList.add(new Statement(serial+13000L,1,"<el-form  :model=\"form\" label-position=\"right\" label-width=\"120px\">"));
			sList.add(new Statement(serial+14000L,1,"<el-input v-model=\"form.id\" type=\"hidden\"></el-input>"));
			sList.add(new Statement(serial+15000L,1,"<el-row>"));
			serial = serial + 16000L;
			for (FrontField f:this.domain.getAllFieldsWithoutId()) {
				if (!(f instanceof FrontDropdown)) {
					if (f.getFieldType().getTypeName().equalsIgnoreCase("image")) {
						sList.add(new Statement(serial+1000L,1,"<el-col :span=\"12\">"));
						sList.add(new Statement(serial+2000L,2,"<el-form-item label=\""+f.getText()+"\">"));
						sList.add(new Statement(serial+3000L,1,"<el-upload"));
						sList.add(new Statement(serial+4000L,2,":action=\"this.baseApi+'"+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/addUpload"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"'\""));
						sList.add(new Statement(serial+5000L,2,":show-file-list=\"false\""));
						sList.add(new Statement(serial+6000L,2,":on-success=\"handle"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"ImageSuccess\""));
						sList.add(new Statement(serial+7000L,2,":before-upload=\"before"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"ImageUpload\">"));
						sList.add(new Statement(serial+8000L,4,"<img v-if=\""+f.getLowerFirstFieldName()+"ImageUrl\" :src=\""+f.getLowerFirstFieldName()+"ImageUrl\" width=\"300\"/>"));
						sList.add(new Statement(serial+8500L,4,"<img v-else-if=\"!form."+f.getLowerFirstFieldName()+"\"　:src=\"'../src/views/images/blank.jpg'\" width=\"300\"/>"));
						sList.add(new Statement(serial+9000L,4,"<img v-else :src=\"'data:image/png;base64,'+form."+f.getLowerFirstFieldName()+"\" width=\"300\"/>"));
						sList.add(new Statement(serial+10000L,1,"<br>"));
						if (this.domain.getLanguage().equalsIgnoreCase("english")) {
							sList.add(new Statement(serial+11000L,4,"<el-button type=\"primary\" plain v-on:click=\"up"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"ImageData\">Upload</el-button>"));
						}else {  
							sList.add(new Statement(serial+11000L,4,"<el-button type=\"primary\" plain v-on:click=\"up"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"ImageData\">上传</el-button>"));
						}
						sList.add(new Statement(serial+12000L,1,"</el-upload>"));
						sList.add(new Statement(serial+13000L,2,"</el-form-item>"));
						sList.add(new Statement(serial+14000L,2,"</el-col>"));
					}else {
						sList.add(new Statement(serial,2,"<el-col :span=\"12\">"));
						sList.add(new Statement(serial+1000L,2,"<el-form-item label=\""+f.getText()+"\">"));
						sList.add(new Statement(serial+2000L,3,"<el-input v-model=\"form."+f.getFieldName()+"\" style=\"width: 240px;\"></el-input>"));
						sList.add(new Statement(serial+3000L,2,"</el-form-item>"));
						sList.add(new Statement(serial+4000L,2,"</el-col>"));
					}
					serial = serial + 15000L;
				}else {
					FrontDropdown dp = (FrontDropdown)f;
					sList.add(new Statement(serial,2,"<el-col :span=\"12\">"));
					sList.add(new Statement(serial+1000L,2,"<el-form-item label=\""+dp.getText()+"\" prop=\""+dp.getAliasOrName()+"\">"));
					if (this.domain.getLanguage().equalsIgnoreCase("english")) {
						sList.add(new Statement(serial+2000L,3,"<el-select v-model=\"form."+dp.getAliasName()+"\" value-key=\""+dp.getTarget().getDomainId().getFieldName()+"\" placeholder=\"Please choose \" style=\"width: 240px;\" @change=\"translate"+dp.getTarget().getCapFirstDomainName()+"\">"));
					}else {
						sList.add(new Statement(serial+2000L,3,"<el-select v-model=\"form."+dp.getAliasName()+"\" value-key=\""+dp.getTarget().getDomainId().getFieldName()+"\" placeholder=\"请选择\" style=\"width: 240px;\" @change=\"translate"+dp.getTarget().getCapFirstDomainName()+"\">"));
					}
					sList.add(new Statement(serial+3000L,3,"<el-option"));
					sList.add(new Statement(serial+4000L,4,"v-for=\"item in active"+dp.getTarget().getCapFirstPlural()+"\""));
					sList.add(new Statement(serial+5000L,4,":key=\"item."+dp.getAliasName()+"\""));
					sList.add(new Statement(serial+6000L,4,":label=\"item."+dp.getTarget().getDomainName().getFieldName()+"\""));
					sList.add(new Statement(serial+7000L,4,":value=\"item."+dp.getTarget().getDomainId().getFieldName()+"\">	"));
					sList.add(new Statement(serial+8000L,3,"</el-option>"));
					sList.add(new Statement(serial+9000L,3,"</el-select>"));
					sList.add(new Statement(serial+10000L,2,"</el-form-item>"));
					sList.add(new Statement(serial+11000L,2,"</el-col>"));
					serial = serial + 15000L;
				}
			}
			sList.add(new Statement(serial,1,"</el-row>"));
			sList.add(new Statement(serial+1000L,1,"</el-form>"));
			sList.add(new Statement(serial+2000L,1,"<span slot=\"footer\" class=\"dialog-footer\">"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sList.add(new Statement(serial+3000L,1,"<el-button @click=\"editDialogVisible = false\">Cancel</el-button>"));
				sList.add(new Statement(serial+4000L,1,"<el-button type=\"primary\" @click=\"update"+this.domain.getCapFirstDomainName()+"\">Update</el-button>"));
			}else {
				sList.add(new Statement(serial+3000L,1,"<el-button @click=\"editDialogVisible = false\">取 消</el-button>"));
				sList.add(new Statement(serial+4000L,1,"<el-button type=\"primary\" @click=\"update"+this.domain.getCapFirstDomainName()+"\">编辑</el-button>"));
			}
			sList.add(new Statement(serial+5000L,1,"</span>"));
			sList.add(new Statement(serial+6000L,0,"</el-dialog>"));
		}
		
		if (!this.domain.isVerbDenied("View")) {
			sList.add(new Statement(serial+8000L,0,"<el-dialog"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sList.add(new Statement(serial+9000L,1,"title=\"View\""));
			}else {
				sList.add(new Statement(serial+9000L,1,"title=\"查看\""));
			}
			sList.add(new Statement(serial+10000L,1,":visible.sync=\"viewDialogVisible\""));
			sList.add(new Statement(serial+11000L,1,"width=\"850px\""));
			sList.add(new Statement(serial+12000L,1,":before-close=\"handleClose\">"));
			sList.add(new Statement(serial+13000L,1,"<el-form  :model=\"form\" label-position=\"right\" label-width=\"120px\">"));
			sList.add(new Statement(serial+14000L,1,"<el-input v-model=\"form.id\" type=\"hidden\"></el-input>"));
			sList.add(new Statement(serial+15000L,1,"<el-row>"));
			serial = serial + 16000L;
			for (FrontField f:this.domain.getAllFieldsWithoutId()) {
				if (!(f instanceof FrontDropdown)) {
					if (f.getFieldType().getTypeName().equalsIgnoreCase("image")) {
						sList.add(new Statement(serial+1000L,0,"<el-col :span=\"12\">"));
						sList.add(new Statement(serial+2000L,2,"<el-form-item label=\""+f.getText()+"\">"));
						sList.add(new Statement(serial+3000L,4,"<img v-if=\"!form."+f.getLowerFirstFieldName()+"\"　:src=\"'../src/views/images/blank.jpg'\" width=\"300\"/>"));
						sList.add(new Statement(serial+4000L,4,"<img v-else :src=\"'data:image/png;base64,'+form."+f.getLowerFirstFieldName()+"\" width=\"300\"/>"));
						sList.add(new Statement(serial+5000L,2,"</el-form-item>"));
						sList.add(new Statement(serial+6000L,2,"</el-col>"));
					}else {
						sList.add(new Statement(serial,2,"<el-col :span=\"12\">"));
						sList.add(new Statement(serial+1000L,2,"<el-form-item label=\""+f.getText()+"\">"));
						sList.add(new Statement(serial+2000L,3,"<el-input v-model=\"form."+f.getFieldName()+"\" style=\"width: 240px;\"></el-input>"));
						sList.add(new Statement(serial+3000L,2,"</el-form-item>"));
						sList.add(new Statement(serial+4000L,2,"</el-col>"));
					}
					serial = serial + 12000L;
				}else {
					FrontDropdown dp = (FrontDropdown)f;
					sList.add(new Statement(serial,2,"<el-col :span=\"12\">"));
					sList.add(new Statement(serial+1000L,2,"<el-form-item label=\""+dp.getText()+"\" prop=\""+dp.getAliasOrName()+"\">"));
					if (this.domain.getLanguage().equalsIgnoreCase("english")) {
						sList.add(new Statement(serial+2000L,3,"<el-select v-model=\"form."+dp.getAliasName()+"\" value-key=\""+dp.getTarget().getDomainId().getFieldName()+"\" placeholder=\"Please choose \" style=\"width: 240px;\" @change=\"translate"+dp.getTarget().getCapFirstDomainName()+"\">"));
					}else {
						sList.add(new Statement(serial+2000L,3,"<el-select v-model=\"form."+dp.getAliasName()+"\" value-key=\""+dp.getTarget().getDomainId().getFieldName()+"\" placeholder=\"请选择\" style=\"width: 240px;\" @change=\"translate"+dp.getTarget().getCapFirstDomainName()+"\">"));
					}
					sList.add(new Statement(serial+3000L,3,"<el-option"));
					sList.add(new Statement(serial+4000L,4,"v-for=\"item in active"+dp.getTarget().getCapFirstPlural()+"\""));
					sList.add(new Statement(serial+5000L,4,":key=\"item."+dp.getAliasName()+"\""));
					sList.add(new Statement(serial+6000L,4,":label=\"item."+dp.getTarget().getDomainName().getFieldName()+"\""));
					sList.add(new Statement(serial+7000L,4,":value=\"item."+dp.getTarget().getDomainId().getFieldName()+"\">	"));
					sList.add(new Statement(serial+8000L,3,"</el-option>"));
					sList.add(new Statement(serial+9000L,3,"</el-select>"));
					sList.add(new Statement(serial+10000L,2,"</el-form-item>"));
					sList.add(new Statement(serial+11000L,2,"</el-col>"));
					serial = serial + 12000L;
				}
			}
			sList.add(new Statement(serial,1,"</el-row>"));
			sList.add(new Statement(serial+1000L,1,"</el-form>"));
			sList.add(new Statement(serial+2000L,1,"<span slot=\"footer\" class=\"dialog-footer\">"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sList.add(new Statement(serial+3000L,1,"<el-button @click=\"viewDialogVisible = false\">Close</el-button>"));
			}else {
				sList.add(new Statement(serial+3000L,1,"<el-button @click=\"viewDialogVisible = false\">关　闭</el-button>"));
			}
			sList.add(new Statement(serial+5000L,1,"</span>"));
			sList.add(new Statement(serial+6000L,0,"</el-dialog>"));
		}
		
		sList.add(new Statement(serial+7000L,0,""));
		sList.add(new Statement(serial+8000L,0,"</div>"));
		sList.add(new Statement(serial+9000L,0,"</template>"));
		sList.add(new Statement(serial+10000L,0,"<script>"));
		sList.add(new Statement(serial+11000L,0,"import {"));
		
		StringBuilder sb = new StringBuilder("search"+this.domain.getCapFirstPlural()+"ByFieldsByPage,");
		if (!this.domain.isVerbDenied("SoftDelete")) sb.append("softDelete"+this.domain.getCapFirstDomainName()+",");	
		if (!this.domain.isVerbDenied("Activate")) sb.append("activate"+this.domain.getCapFirstDomainName()+",");	
		if (!this.domain.isVerbDenied("Toggle")) sb.append("toggle"+this.domain.getCapFirstDomainName()+",");	
		if (!this.domain.isVerbDenied("ToggleOne")) sb.append("toggleOne"+this.domain.getCapFirstDomainName()+",");	
		if (!this.domain.isVerbDenied("Delete")) sb.append("delete"+this.domain.getCapFirstDomainName()+",");	
		if (!this.domain.isVerbDenied("SoftDeleteAll")) sb.append("softDeleteAll"+this.domain.getCapFirstPlural()+",");	
		if (!this.domain.isVerbDenied("DeleteAll")) sb.append("deleteAll"+this.domain.getCapFirstPlural()+",");	
		if (!this.domain.isVerbDenied("ActivateAll")) sb.append("activateAll"+this.domain.getCapFirstPlural()+",");	
		if (!this.domain.isVerbDenied("Add")) sb.append("add"+this.domain.getCapFirstDomainName()+",");	
		if (!this.domain.isVerbDenied("Update")) sb.append("update"+this.domain.getCapFirstDomainName()+",");	
		if (!this.domain.isVerbDenied("Clone")) sb.append("clone"+this.domain.getCapFirstDomainName()+",");	
		if (!this.domain.isVerbDenied("CloneAll")) sb.append("cloneAll"+this.domain.getCapFirstPlural()+",");	
		
		sList.add(new Statement(serial+12000L,0,sb.substring(0,sb.length()-1)));
		sList.add(new Statement(serial+13000L,0,"} from '@/api/"+this.domain.getPlural().toLowerCase()+"'"));
		Set<FrontDomain> dpDomains = new TreeSet<FrontDomain>();
		FrontVar defaultForm = new FrontVar("defaultForm");
		FrontVar postForm = new FrontVar("postForm");
		FrontVar listQuery = new FrontVar("listQuery");
		FrontVar pagevars = new FrontVar("pagevars");
		FrontVar data = new FrontVar("data");	
		for (FrontField ff:this.domain.getAllFieldsWithoutId()) {
			if (ff instanceof FrontDropdown) {
				FrontDropdown dp = (FrontDropdown) ff;
				dpDomains.add(dp.getTarget());
			}
		}
		serial += 14000L;
		for (FrontDomain d:dpDomains) {
			sList.add(new Statement(serial,0,"import {"));
			sList.add(new Statement(serial+1000L,0,"listActive"+d.getCapFirstPlural()+""));
			sList.add(new Statement(serial+2000L,0,"} from '@/api/"+d.getPlural().toLowerCase()+"'"));
			serial += 3000L;
		}
		sList.add(new Statement(serial,0,""));
		sList.add(new Statement(serial+1000L,0,"import pagevars from '@/pagevars'"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,0,"import Pagination from '@/components/Pagination' // Secondary package based on el-pagination"));
		sList.add(new Statement(serial+4000L,0,""));
		StatementList sl = UtilsMethodGenerator.generateDefaultFormBlockStatements(this.domain, defaultForm);
		sl.setSerial(serial+5000L);
		sList.add(sl);
		serial += 6000L;
		sList.add(new Statement(serial,0,""));
		sList.add(new Statement(serial+1000L,0,"export function isBlank(val) {"));
		sList.add(new Statement(serial+2000L,0,"if (val == undefined || val == null || val == \"\" ) return true;"));
		sList.add(new Statement(serial+3000L,0,"else return false;"));
		sList.add(new Statement(serial+4000L,0,"}"));
		sList.add(new Statement(serial+5000L,0,""));
		sList.add(new Statement(serial+6000L,0,"export default {"));
		sList.add(new Statement(serial+7000L,0,"inject:['reload'],"));
		sList.add(new Statement(serial+8000L,0,"name: '"+this.domain.getCapFirstPlural()+"List',"));
		sList.add(new Statement(serial+9000L,0,"components: {"));
		sList.add(new Statement(serial+10000L,1,"Pagination"));
		sList.add(new Statement(serial+11000L,0,"},"));
		sList.add(new Statement(serial+12000L,0,"filters: {"));
		serial += 13000L;
		for (FrontDomain d:dpDomains) {
			sl = UtilsMethodGenerator.generateDomainFilterStatements(d, pagevars);
			sl.setSerial(serial);
			sList.add(sl);
			serial += 1000L;
		}
		sList.add(new Statement(serial,0,"},"));
		
		sl = UtilsMethodGenerator.generateDataBlockStatements(domain, listQuery, postForm, defaultForm, pagevars, dpDomains);
		sl.setSerial(serial+1000L);
		sList.add(sl);
		
		sl = UtilsMethodGenerator.generateCreatedBlockStatements(domain, dpDomains);
		sl.setSerial(serial+2000L);
		sList.add(sl);
		
		sList.add(new Statement(serial+3000L,0,"methods: {"));
		
		sl = UtilsMethodGenerator.generateSearchFilterFunMethodStatements(this.domain,!this.domain.isVerbDenied("FilterExcel"),!this.domain.isVerbDenied("FilterPDF"),!this.domain.isVerbDenied("FilterWord"));
		sl.setSerial(serial+4000L);
		sList.add(sl);
		
		sl = UtilsMethodGenerator.generateToQueryStringMethodStatements();
		sl.setSerial(serial+5000L);
		sList.add(sl);
		
		serial+=6000L;
		for (FrontField f: domain.getFields()) {
			if (f.getFieldType()!=null&&f.getFieldType().getTypeName()!=null&&"image".equalsIgnoreCase(f.getFieldType().getTypeName())) {
				sl = UtilsMethodGenerator.generateHandleImageSuccessMethodStatements(domain, f);
				sl.setSerial(serial);
				sList.add(sl);
				sl = UtilsMethodGenerator.generateBeforeImageUploadsMethodStatements(domain, f);
				sl.setSerial(serial+1000L);
				sList.add(sl);
				sl = UtilsMethodGenerator.generateUpImageDataMethodStatements(domain, f);
				sl.setSerial(serial+2000L);
				sList.add(sl);
				serial+= 3000L;
			}				
		}
		
		sl = UtilsMethodGenerator.generateBuildParamsStatements(domain, listQuery, postForm);
		sl.setSerial(serial+4000L);
		sList.add(sl);
		
		if (domain.getDomainId()!= null) {
			sl = UtilsMethodGenerator.generateBuildIdsStatements(domain, data);
			sl.setSerial(serial+5000L);
			sList.add(sl);	
		}
		
		if (!this.domain.isVerbDenied("Delete")) {			
			sl = UtilsMethodGenerator.generateConfirmDeleteMethodStatements(this.domain);
			sl.setSerial(serial+6000L);
			sList.add(sl);
		}
		
		if (!this.domain.isVerbDenied("DeleteAll")) {	
			sl = UtilsMethodGenerator.generateConfirmDeleteAllMethodStatements(this.domain);
			sl.setSerial(serial+7000L);
			sList.add(sl);
		}
		
		sl = UtilsMethodGenerator.generateHandleSizeChangeMethodStatements(listQuery);
		sl.setSerial(serial+8000L);
		sList.add(sl);
		
		sl = UtilsMethodGenerator.generateHandleCurrentChangeMethodStatements(listQuery);
		sl.setSerial(serial+9000L);
		sList.add(sl);
		
		sl = UtilsMethodGenerator.generateHandleCheckedMethodStatements();
		sl.setSerial(serial+10000L);
		sList.add(sl);
		
		sl = UtilsMethodGenerator.generateHandleCheckedAllMethodStatements(this.domain);
		sl.setSerial(serial+11000L);
		sList.add(sl);
		
		sl = UtilsMethodGenerator.generateSelectedHighlightMethodStatements();
		sl.setSerial(serial+12000L);
		sList.add(sl);
		
		sl = UtilsMethodGenerator.generateHandleCloseMethodStatements(this.domain.getLanguage());
		sl.setSerial(serial+13000L);
		sList.add(sl);
		
		sl = UtilsMethodGenerator.generateShowViewDialogMethodStatements();
		sl.setSerial(serial+13500L);
		sList.add(sl);	
		
		sl = UtilsMethodGenerator.generateShowAddDialogMethodStatements();
		sl.setSerial(serial+14000L);
		sList.add(sl);		
		
		sl = UtilsMethodGenerator.generateShowEditDialogMethodStatements();
		sl.setSerial(serial+15000L);
		sList.add(sl);		
		
		serial += 16000L;
		for (FrontVerb fv: this.verbs ) {
			FrontMethod m = fv.generateControllerMethod();
			if (m!= null) {
				m.setSerial(serial);
				sList.add(m.getMethodStatementList());
				serial += 1000L;
			}
		}
		
		for (FrontTwinsVerb tv: this.twinsverbs) {
			FrontMethod m = tv.generateControllerMethod();
			if (m!= null) {
				m.setSerial(serial);
				sList.add(m.getMethodStatementList());
				serial += 1000L;
			}
		}

		for (FrontDomain dpd: dpDomains ) {
			ListActive la = new ListActive(dpd);
			FrontMethod m = la.generateControllerMethod();
			if (m!= null) {
				m.setSerial(serial);
				sList.add(m.getMethodStatementList());
				serial += 1000L;
			}
		}
		sl = UtilsMethodGenerator.generateSleepMethodStatements();
		sl.setSerial(serial);
		sList.add(sl);
		
		serial += 1000L;
		for (FrontDropdown dp : this.domain.getDropdownSet()) {
			sl = UtilsMethodGenerator.generateTranslateDomainMethodStatements(dp.getTarget());
			sl.setSerial(serial);
			sList.add(sl);
			serial += 1000L;
		}
		
		sList.add(new Statement(serial+1000L,0,"},"));
		sList.add(new Statement(serial+2000L,0,"watch: {"));
		sList.add(new Statement(serial+3000L,1,"selectList(data) {"));
		sList.add(new Statement(serial+4000L,1,"this.selectRows = [];"));
		sList.add(new Statement(serial+5000L,1,"if (data.length > 0) {"));
		sList.add(new Statement(serial+6000L,2,"data.forEach((item, rowindex) => {"));
		sList.add(new Statement(serial+7000L,2,"this.selectRows.push(this.list.indexOf(item));"));
		sList.add(new Statement(serial+8000L,2,"});"));
		sList.add(new Statement(serial+9000L,1,"}"));
		sList.add(new Statement(serial+10000L,1,"this.selectRows.sort();"));
		sList.add(new Statement(serial+11000L,1,"}"));
		sList.add(new Statement(serial+12000L,0,"}"));
		sList.add(new Statement(serial+13000L,0,"}"));
		sList.add(new Statement(serial+14000L,0,"</script>"));
		return WriteableUtil.merge(sList);
	}

	public Set<FrontTwinsVerb> getTwinsverbs() {
		return twinsverbs;
	}

	public void setTwinsverbs(Set<FrontTwinsVerb> twinsverbs) {
		this.twinsverbs = twinsverbs;
	}

}
